# MiniSAT with XorEngine and Pseudo-Boolean Proof Logging

MiniSAT with XorEngine is the SAT solver MiniSAT enhanced with an XorEngine for parity reasoning. The main purpose of this project is to demonstrate that proof logging for parity reasoning is easy using the pseudo-Boolean proof format used by proof checker MIAOresearch/software/VeriPB>.

## Building

The Makefile takes care of downloading the XorEngine submodule and compiling MiniSAT with the XorEngine. To compile MiniSAT with XorEngine and pseudo-Boolean proof logging, run:
```bash
make pbp
```
The compiled binary will be placed in the `bin` directory.

## Running with Proof Logging Enabled

MiniSAT with XorEngine supports the same command line options a MiniSAT. To run the solver with proof logging, use:
```bash
./minisat_pbp -proof-file=instance.pbp instance.cnf
```

This will solve the SAT instance `instance.cnf` and write the pseudo-Boolean proof to `instance.pbp`.

## License
All the code in this repository is licensed under the [MIT License](LICENSE)


## Original MiniSAT README
<details>
<summary>Click here for the original README</summary>

```
================================================================================
DIRECTORY OVERVIEW:

mtl/            Mini Template Library
utils/          Generic helper code (I/O, Parsing, CPU-time, etc)
core/           A core version of the solver
simp/           An extended solver with simplification capabilities
README
LICENSE

================================================================================
BUILDING: (release version: without assertions, statically linked, etc)

export MROOT=<minisat-dir>              (or setenv in cshell)
cd { core | simp }
gmake rs
cp minisat_static <install-dir>/minisat

================================================================================
EXAMPLES:

Run minisat with same heuristics as version 2.0:

> minisat <cnf-file> -no-luby -rinc=1.5 -phase-saving=0 -rnd-freq=0.02
```

</details>
